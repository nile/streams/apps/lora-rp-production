package ch.cern.nile.app;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.common.streams.StreamUtils;
import ch.cern.nile.common.streams.offsets.InjectOffsetProcessorSupplier;

public final class RpProductionStream extends AbstractStream {

    private final Map<String, Long> timestamps = new HashMap<>();

    @Override
    public void createTopology(final StreamsBuilder builder) {
        builder
                .stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde()))
                .filter(this::filterRecord)
                .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
                .flatMapValues(this::mapValues)
                .filter(StreamUtils::filterNull)
                .to(getSinkTopic());
    }

    private boolean filterRecord(final String ignored, final JsonObject value) {
        return value.get("fPort") != null
                && value.get("data") != null
                && value.get("rxInfo") != null
                && value.get("deviceName") != null;
    }

    private Collection<Map<String, Object>> mapValues(final JsonObject value) {
        Collection<Map<String, Object>> collection;
        try {
            final String deviceName = value.get("deviceName").getAsString();
            final Long lastDeviceTimestamp = timestamps.getOrDefault(deviceName, null);
            collection = RpProductionDecoder.decode(value, lastDeviceTimestamp);
            timestamps.put(deviceName, System.currentTimeMillis());
            setLastReadOffset(value.get("offset").getAsLong());
        } catch (IOException | ParseException | DecodingException e) {
            logStreamsException(e);
            collection = Collections.emptyList();
        }
        return collection;
    }

}
